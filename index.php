<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Live Birth | 2016</title>

        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" id="bootstrap-css">
        <link href="assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="bg-glow">
        <section>
            <div class="container">
                <div class="login-container">
                    <div id="output"></div>
                    <div class="avatar">
                        <img src="assets/img/baby.png" />
                    </div>
                    <div class="form-box">
                        <a href="./view/registration/child/index.php"><button class="btn btn-info text-md text-dark-dk" type="submit">Registered baby List</button></a>
                        <a href="./view/registration/child/create.php"><button class="btn btn-success m-t-lg text-md text-dark-dk" type="submit">New baby Register</button></a>

                    </div>
                </div>

            </div>
        </section>
        


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>


    </body>
</html>