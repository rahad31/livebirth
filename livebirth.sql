-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2016 at 08:53 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `livebirth`
--

-- --------------------------------------------------------

--
-- Table structure for table `child`
--

CREATE TABLE IF NOT EXISTS `child` (
  `id` int(11) NOT NULL,
  `cfname` varchar(64) NOT NULL,
  `clname` varchar(64) NOT NULL,
  `sex` text NOT NULL,
  `date` date NOT NULL,
  `religion` text NOT NULL,
  `location` varchar(127) NOT NULL,
  `nation` text NOT NULL,
  `cnumber` bigint(20) NOT NULL,
  `title` varchar(64) NOT NULL,
  `ffname` varchar(64) NOT NULL,
  `flname` varchar(64) NOT NULL,
  `occupation` text NOT NULL,
  `contact` bigint(20) NOT NULL,
  `address` text NOT NULL,
  `mfname` varchar(64) NOT NULL,
  `mlname` varchar(64) NOT NULL,
  `moccupation` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `child`
--

INSERT INTO `child` (`id`, `cfname`, `clname`, `sex`, `date`, `religion`, `location`, `nation`, `cnumber`, `title`, `ffname`, `flname`, `occupation`, `contact`, `address`, `mfname`, `mlname`, `moccupation`) VALUES
(2, 'Rony', 'talukdar', 'male', '2016-01-12', 'islam', 'Comilla', 'bangladeshi', 4578961230, 'Mr', 'Jamal', 'Talukdar', 'Govt Officer', 1739586452, 'Rampura', 'Roksana', 'Begum', 'Housewife');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `child`
--
ALTER TABLE `child`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `child`
--
ALTER TABLE `child`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
