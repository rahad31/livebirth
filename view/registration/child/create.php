<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Live Birth | 2016</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font awsome-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
    <body class="bg-info">

        <section>
            <div class="container">
                
                    <h2>Bangladesh Standard Birth Certificate</h2>
                    <form class="" role="form" action="store.php" method="POST">
                    <div class="row "> 
                        <div class="col-md-5 ">
                            <div class="row">
                                <h3>Child's Details</h3>
                                <div class="col-sm-6">
                                    <label for="cfname">First Name</label><br/>
                                    <input type="text" class="form-control" name="cfname"  placeholder="" required="">
                                </div>

                                <div class="col-sm-6">
                                    <label for="clname">Last Name</label><br/>
                                    <input type="text" class="form-control" name="clname"  placeholder="" required="">
                                </div>

                                <div class="col-sm-6">
                                    <label for="sex">Sex</label><br/>
                                    <select name="sex">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label for="date">Date of Birth (Mo/Day/Yr)</label><br/>
                                    <input type="date" class="form-control" name="date" placeholder="" required="">
                                </div>

                                <div class="col-sm-12">
                                    <label for="religion">Religion</label><br/>
                                    <input type="text" class="form-control" name="religion" placeholder="" required="">
                                </div>

                                <div class="col-sm-12">
                                    <label for="location">Location of Birth</label><br/>
                                    <input type="text" class="form-control" name="location"  placeholder="" required="">
                                </div>
                                <div class="col-sm-12">
                                    <label for="nation">Nationality</label><br/>
                                    <input type="text" class="form-control" name="nation" placeholder="" required="">
                                </div>
                                <div class="col-sm-12">
                                    <label for="cnumber">Child identification number</label><br/>
                                    <input type="text" class="form-control" name="cnumber" placeholder="" required="">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-5 col-md-offset-2">

                            <!-- Fathers Details -->

                            <div class="row">
                                <h3>Father's Details</h3>

                                <div class="radio">
                                    <label for="title">Title: </label>
                                    <label><input name="title" type="radio" value="MD"> MD</label>
                                    <label><input name="title" type="radio" value="Mr"> Mr</label>
                                </div>
                                <div class="col-sm-6">
                                    <label for="ffname">First Name</label><br/>
                                    <input type="text" class="form-control" name="ffname"  placeholder="" required="">
                                </div>

                                <div class="col-sm-6">
                                    <label for="flname">Last Name</label><br/>
                                    <input type="text" class="form-control" name="flname"  placeholder="" required="">
                                </div>

                                <div class="col-sm-6">
                                    <label for="occupation">Occupation</label><br/>
                                    <input type="text" class="form-control" name="occupation"  placeholder="" required="">
                                </div>
                                <div class="col-sm-6">
                                    <label for="contact">Contact No.</label><br/>
                                    <input type="text" class="form-control" name="contact"  placeholder="" required="">
                                </div>
                                <div class="col-sm-12">
                                    <label for="address">Address</label><br/>
                                    <input type="text" class="form-control" name="address" id="birthdate" placeholder="" required="">
                                </div>
                            </div>

                            <!--Mother-->
                            <div class="row">
                                <h3>Mother's Details</h3>

                                <div class="col-sm-6">
                                    <label for="mfname">First Name</label><br/>
                                    <input type="text" class="form-control" name="mfname" id="fname" placeholder="" required="">
                                </div>

                                <div class="col-sm-6">
                                    <label for="mlname">Last Name</label><br/>
                                    <input type="text" class="form-control" name="mlname" id="lname" placeholder="" required="">
                                </div>
                                <div class="col-sm-12">
                                    <label for="moccupation">Occupation</label><br/>
                                    <input type="text" class="form-control" name="moccupation" id="birthdate" placeholder="" required="">
                                </div>
                            </div>

                        </div>

                    </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>
        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../assets/js/bootstrap.min.js"></script>


    </body>
</html>