<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'livebirth' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

use App\registration\child\Child;
use App\registration\Utility\Utility;

$ccc = new Child();
$info = $ccc->show($_GET['id']);
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Live Birth | 2015</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <section>
            <div class="container">

                <h2 class="text-ash text-center">Bangladesh Standard Birth Certificate</h2>

                <div class="row "> 
                    <div class="col-md-5 ">
                        <div class="row">
                            <h3>Child's Details</h3>
                            <div class="col-sm-6">
                                <label for="cfname">First Name</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->cfname; ?></span>
                            </div>

                            <div class="col-sm-6">
                                <label for="clname">Last Name</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->clname; ?></span>
                            </div>

                            <div class="col-sm-6">
                                <label for="sex">Sex</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->sex; ?></span>
                            </div>
                            <div class="col-sm-6">
                                <label for="date">Date of Birth (Day/Mo/Yr)</label><br/>
                                <span class="text-md text-green-lt"><?php echo date('d-m-Y', strtotime($info->date)); ?></span>
                            </div>

                            <div class="col-sm-12">
                                <label for="religion">Religion</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->religion; ?></span>
                            </div>

                            <div class="col-sm-12">
                                <label for="location">Location of Birth</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->location; ?></span>
                            </div>
                            <div class="col-sm-12">
                                <label for="nation">Nationality</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->nation; ?></span>
                            </div>
                            <div class="col-sm-12">
                                <label for="cnumber">Child identification number</label><br/>
                                <strong><span class="text-md text-green-lt"><?php echo $info->cnumber; ?></span></strong>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-5 col-md-offset-2">

                        <!-- Fathers Details -->

                        <div class="row">
                            <h3>Father's Details</h3>

                            <div class="radio">
                                <label for="title">Title: </label>
                                <span class="text-md text-green-lt"><?php echo $info->title; ?></span>
                            </div>
                            <div class="col-sm-6">
                                <label for="ffname">First Name</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->ffname; ?></span>
                            </div>

                            <div class="col-sm-6">
                                <label for="flname">Last Name</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->flname; ?></span>
                            </div>

                            <div class="col-sm-6">
                                <label for="occupation">Occupation</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->occupation; ?></span>
                            </div>
                            <div class="col-sm-6">
                                <label for="contact">Contact No.</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->contact; ?></span>
                            </div>
                            <div class="col-sm-12">
                                <label for="address">Address</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->address; ?></span>
                            </div>
                        </div>

                        <!--Mother-->
                        <div class="row">
                            <h3>Mother's Details</h3>

                            <div class="col-sm-6">
                                <label for="mfname">First Name</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->mfname; ?></span>
                            </div>

                            <div class="col-sm-6">
                                <label for="mlname">Last Name</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->mlname; ?></span>
                            </div>
                            <div class="col-sm-12">
                                <label for="moccupation">Occupation</label><br/>
                                <span class="text-md text-green-lt"><?php echo $info->moccupation; ?></span>
                            </div>
                        </div>

                    </div>

                </div>

        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../assets/js/bootstrap.min.js"></script>


    </body>
</html>