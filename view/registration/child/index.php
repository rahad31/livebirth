<?php 
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'livebirth'. DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

use \App\registration\child\Child;
use \App\registration\Utility\Utility;

$ccc = new Child();

$days = $ccc->index();


?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Live Birth | 2015</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <section>
            <div class="container">
                <div id="msg" style="background-color: #46b8da; color: #F00; font-size: 25px;">
                <?php echo Utility::message(); ?>            
            </div>
                <table class="table table-bordered table-hover text-center bg-info">
                <thead >
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Childname</th>
                        <th colspan="3" class="text-center">Action</th>

                    </tr>
                </thead>
                <tbody>
                     <?php 
                        $srl = 1;
                        foreach ($days as $day){
                        ?>
                    <tr>
                        <td><?php echo $day->id ?></td>
                        <td><?php echo $day->cfname; ?></td>
                        <td><a href="show.php?id=<?php echo $day->id ?>" class="btn btn-success ">View</a></td>
                        <td><a href="edit.php?id=<?php echo $day->id ?>" class="btn btn-primary ">Edit</a></td>
                        <td>
                            <form action="delete.php" method="POST">
                                    <input type="hidden" name="id" value="<?php echo $day->id; ?>"/>
                                    <button class="btn btn-danger delete" type="submit" >Delete</button>
                                </form>
                        </td>
                    </tr>
                    <?php 
                        }
                       
                      ?>
                </tbody>
            </table>
            </div>
        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../assets/js/bootstrap.min.js"></script>
        
        <script>
            $('.delete').bind('click',function(e){
                var dlt = confirm("Are you sure to Delete book Title");
                if(!dlt){
                    e.preventDefault();
                }
            });
            $('#msg').fadeOut(5000);
        </script>

    </body>
</html>