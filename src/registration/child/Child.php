<?php

namespace App\registration\child;

use App\registration\Utility\Utility;
use App\registration\Model\Model;

class Child extends Model {

    public $table = "child";

//    public $id = "";
//    public $cfname = "";
//    public $clname = "";
//    public $sex = "";
//    public $date = "";
//    public $religion = "";
//    public $location = "";
//    public $nation = "";
//    public $cnumber = "";
//    public $title = "";
//    public $ffname = "";
//    public $flname = "";
//    public $occupation = "";
//    public $contact = "";
//    public $address = "";
//    public $mfname = "";
//    public $mlname = "";
//    public $moccupation = "";

    public function __construct($data = false) {

        parent::__construct();
    }

    public function index() {
        $child = array();

        $query = "SELECT * FROM `child`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $child[] = $row;
        }
        return $child;
    }

    public function show($id = false) {


        $query = "SELECT * FROM `$this->table` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_object($result);
        return $row;
    }

    public function store($data = array()) {

        $data = array('child' => $data);

        if ($this->insert($data)) {
            Utility::message("Child Info is inserted successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function edit($data = array()) {
        
        $data = array('child' => $data);
        
////        $query = "UPDATE `$this->table` SET `title` = '" . $this->title . "' WHERE `books`.`ID` = " . $this->id;
//        $result = mysql_query($query);
        if ($this->update($data)) {
            Utility::message("Child Info is updated successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function delete($id = null) {
        if (is_null($id)) {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }


        $query = "DELETE FROM `$this->table` WHERE `$this->table`.`id` = " . $id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Child Detail is successfully Deleted");
        } else {
            Utility::message("can not delete");
        }
        Utility::redirect('index.php');
    }

}
